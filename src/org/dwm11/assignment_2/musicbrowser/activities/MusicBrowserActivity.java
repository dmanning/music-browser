package org.dwm11.assignment_2.musicbrowser.activities;

import java.util.ArrayList;

import org.dwm11.assignment_2.musicbrowser.R;
import org.dwm11.assignment_2.musicbrowser.adapters.AlbumListAdapter;
import org.dwm11.assignment_2.musicbrowser.model.Album;
import org.dwm11.assignment_2.musicbrowser.model.Track;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.ShareActionProvider;
import android.widget.Toast;


public class MusicBrowserActivity extends NavigationEnabledActivity implements OnItemClickListener {

	private static final String TAG = "MusicBrowserActivity";
	private ArrayList<Album> albums = new ArrayList<Album>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate()");
		setContentView(R.layout.music_browser_interface);		
		initialiseAlbumList();
	}
	
	private void initialiseAlbumList(){
		
		//add albums
		albums.add(new Album(R.drawable.ic_launcher,"Back In Black","Rock","1/1/1966","$20",15));
		
		AlbumListAdapter adapter = new AlbumListAdapter(this,R.layout.album_list_item, R.id.album_title, albums);
		ListView album_list = (ListView) findViewById(R.id.album_list);
		album_list.setAdapter(adapter);
		album_list.setOnItemClickListener(this);
		
	}

	/*
	 * Navigation drawer selection handler
	 */
	@Override
	public void onItemClick(AdapterView adapter_view, View clicked_item, int position, long id) {
		Intent intent = new Intent(this,AlbumDetailsActivity.class);
		startActivity(intent);
	}

	/*
	 * Configuration change callback methods
	 */

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		Log.d(TAG, "onConfigurationChanged");
	}
	
	
	/*
	 * Activity lifecycle callback methods
	 */
	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState");
	}


	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		Log.d(TAG, "onPostCreate");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume()");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart()");

	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "onStart()");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}


}
