package org.dwm11.assignment_2.musicbrowser.model;

public class Album {
	
	private int thumbnail_id, track_count;
	private String title, genre, release_date, price;
	
	public Album(int thumbnail_id, String title, String genre, String release_date, String price, int track_count){
		
		this.thumbnail_id = thumbnail_id;
		this.title = title;
		this.genre = genre;
		this.release_date = release_date;
		this.price = price;
		this.track_count = track_count;
	}
	
	public int getThumbnailID(){
		return this.thumbnail_id;
	}
	
	public int getTrackCount(){
		return this.track_count;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getGenre(){
		return this.genre;
	}
	
	public String getPrice(){
		return this.price;
	}
	
	public String getReleaseDate(){
		return this.release_date;
	}
}
