package org.dwm11.assignment_2.musicbrowser.model;

public class Track {
	
	private int track_number, link_icon, play_icon;
	private String title, length, price;
	
	public Track(int link_icon, int play_icon, int track_number, String title, String length, String price){
		this.link_icon = link_icon;
		this.play_icon = play_icon;
		this.track_number = track_number;
		this.title = title;
		this.length = length;
		this.price = price;
	}
	
	public int getTrackNumber(){
		return this.track_number;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getLength(){
		return this.length;
	}
	
	public String getPrice(){
		return this.price;
	}
	
	public int getLinkIcon(){
		return this.link_icon;
	}
	
	public int getPlayIcon(){
		return this.play_icon;
	}

}
