package org.dwm11.assignment_2.musicbrowser.activities;

import java.util.ArrayList;

import org.dwm11.assignment_2.musicbrowser.R;
import org.dwm11.assignment_2.musicbrowser.adapters.AlbumListAdapter;
import org.dwm11.assignment_2.musicbrowser.adapters.TrackListAdapter;
import org.dwm11.assignment_2.musicbrowser.model.Track;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.ShareActionProvider;
import android.widget.Toast;


public class AlbumDetailsActivity extends NavigationEnabledActivity implements OnItemClickListener {

	private static final String TAG = "AlbumDetailsActivity";
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(TAG, "onCreate()");

		setContentView(R.layout.album_details_interface);		
		initialiseTrackList();
	}
	
	private void initialiseTrackList(){
		
		//add albums
		tracks.add(new Track(R.drawable.ic_itunes_available,R.drawable.ic_action_play,1,"Back In Black","1:20","$1.99"));
		
		TrackListAdapter adapter = new TrackListAdapter(this,R.layout.track_list_item, R.id.track_title, tracks);
		ListView track_list = (ListView) findViewById(R.id.track_list);
		track_list.setAdapter(adapter);
		track_list.setOnItemClickListener(this);
		
	}

	/*
	 * Navigation drawer selection handler
	 */
	@Override
	public void onItemClick(AdapterView adapter_view, View clicked_item, int position, long id) {
		Toast.makeText(this, "onItemClick(" + position + ", " + id + ") = " + adapter_view.getAdapter().getItem(position),
				Toast.LENGTH_SHORT).show();
	}


	/*
	 * Configuration change callback methods
	 */

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		Log.d(TAG, "onConfigurationChanged");
	}
	
	
	/*
	 * Activity lifecycle callback methods
	 */
	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState");
	}


	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		Log.d(TAG, "onPostCreate");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume()");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart()");

	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "onStart()");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}


}
