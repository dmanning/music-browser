package org.dwm11.assignment_2.musicbrowser.activities;

import org.dwm11.assignment_2.musicbrowser.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.ShareActionProvider;
import android.widget.Toast;

public class NavigationEnabledActivity extends Activity {
	
	
	private ShareActionProvider share_action_provider;
	private Intent share_intent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setUpShareIntent();
		setUpActionBar();
	}
	
	/*
	 * We will eventually add sharing functionality via the Share ActionBar item.
	 * Create an initial Intent that we'll use to launch the app that will be used to share data.
	 */	
	private void setUpShareIntent() {
		share_intent = new Intent(Intent.ACTION_SEND);
		share_intent.setType("*/*");
	}

	/*
	 * Configure the ActionBar appearance and behaviour.
	 * Add a predefined list of items to the ActionBar navigation drop down list.
	 */	
	private void setUpActionBar() {
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
	}

	/*
	 * Initial configuration for the ActionBar items
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_bar, menu);
		
		MenuItem search_menu_item = (MenuItem) menu.findItem(R.id.action_search);
		SearchView search_view = (SearchView) search_menu_item.getActionView();
		search_view.setIconifiedByDefault(true);
		search_view.setQueryHint(getResources().getString(R.string.search_hint));
		
		share_action_provider = (ShareActionProvider) menu.findItem(R.id.action_share).getActionProvider();
		share_action_provider.setShareHistoryFileName(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
		share_action_provider.setShareIntent(share_intent);
		
		return true;
	}

	/*
	 * Configure the ActionBar items (just before it is displayed)
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.action_search).setVisible(true);
		menu.findItem(R.id.action_share).setVisible(true);
		menu.findItem(R.id.action_sort).setVisible(true);
		return super.onPrepareOptionsMenu(menu);
	}


	/*
	 * ActionBar items selection handler
	 */	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case R.id.action_sort_alpha:
			Toast.makeText(this, "Alpha sort action", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.action_sort_price_asc:
			Toast.makeText(this, "Price asc sort action", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.action_sort_price_desc:
			Toast.makeText(this, "Price desc action", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.action_legal:
			Toast.makeText(this, "Legal notices action", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.settings:
			Toast.makeText(this, "Settings action", Toast.LENGTH_SHORT).show();
			return true;
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
}
