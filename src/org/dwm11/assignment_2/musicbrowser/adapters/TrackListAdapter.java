package org.dwm11.assignment_2.musicbrowser.adapters;

import java.util.List;

import org.dwm11.assignment_2.musicbrowser.R;
import org.dwm11.assignment_2.musicbrowser.model.Track;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TrackListAdapter extends ArrayAdapter<Track> {
	
	private LayoutInflater layout_inflater;
	
	public TrackListAdapter(Context context, int item_layout_id, int default_text_id, List<Track> tracks){
		
		super(context,item_layout_id,default_text_id,tracks);
		layout_inflater = LayoutInflater.from(context);
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		ViewHolder holder;
		if(convertView == null){
			convertView = layout_inflater.inflate(R.layout.track_list_item,null);
			
			holder = new ViewHolder();
			holder.link_icon = (ImageView) convertView.findViewById(R.id.track_link_icon);
			holder.play_icon = (ImageView) convertView.findViewById(R.id.track_play_icon);
			holder.title = (TextView) convertView.findViewById(R.id.track_title);
			holder.price = (TextView) convertView.findViewById(R.id.track_price);
			holder.length = (TextView) convertView.findViewById(R.id.track_length);
			holder.track_number = (TextView) convertView.findViewById(R.id.track_number);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		Track track = this.getItem(position);
		holder.link_icon.setImageResource(track.getLinkIcon());
		holder.link_icon.setImageResource(track.getPlayIcon());
		holder.title.setText(track.getTitle());
		holder.price.setText(track.getPrice());
		holder.length.setText(track.getLength());
		holder.track_number.setText(Integer.toString(track.getTrackNumber()));
		
		return convertView;
	}
	
	static class ViewHolder{
		ImageView link_icon;
		ImageView play_icon;
		TextView title;
		TextView price;
		TextView length;
		TextView track_number;
	}

}
