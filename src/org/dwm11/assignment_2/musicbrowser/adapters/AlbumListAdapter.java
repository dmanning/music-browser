package org.dwm11.assignment_2.musicbrowser.adapters;

import java.util.List;

import org.dwm11.assignment_2.musicbrowser.R;
import org.dwm11.assignment_2.musicbrowser.model.Album;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AlbumListAdapter extends ArrayAdapter<Album> {
	
	private LayoutInflater layout_inflater;
	
	public AlbumListAdapter(Context context, int item_layout_id, int default_text_id, List<Album> albums){
		
		super(context,item_layout_id,default_text_id,albums);
		layout_inflater = LayoutInflater.from(context);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		ViewHolder holder;
		if(convertView == null){
			convertView = layout_inflater.inflate(R.layout.album_list_item,null);
			
			holder = new ViewHolder();
			holder.thumbnail = (ImageView) convertView.findViewById(R.id.album_thumbnail);
			holder.title = (TextView) convertView.findViewById(R.id.album_title);
			holder.price = (TextView) convertView.findViewById(R.id.album_price);
			holder.genre = (TextView) convertView.findViewById(R.id.album_genre);
			holder.release_date = (TextView) convertView.findViewById(R.id.album_release_date);
			holder.track_count = (TextView) convertView.findViewById(R.id.album_track_count);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		Album album = this.getItem(position);
		
		holder.thumbnail.setImageResource(album.getThumbnailID());
		holder.title.setText(album.getTitle());
		holder.price.setText(album.getPrice());
		holder.track_count.setText(Integer.toString(album.getTrackCount()));
		holder.genre.setText(album.getGenre());
		holder.release_date.setText(album.getReleaseDate());
		
		return convertView;
	}
	
	static class ViewHolder{
		ImageView thumbnail;
		TextView title;
		TextView price;
		TextView genre;
		TextView release_date;
		TextView track_count;
	}

}
